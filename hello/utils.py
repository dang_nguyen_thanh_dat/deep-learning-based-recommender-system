import cv2 as cv
from django.conf import settings
from .resnet_50 import resnet50_model


def load_model():
    model_weights_path = "D:/heroku-v1/python-getting-started/hello/17-0.84.hdf5"
    img_width, img_height = 224, 224
    num_channels = 3
    num_classes = 196
    model = resnet50_model(img_height, img_width, num_channels, num_classes)
    model.load_weights(model_weights_path, by_name=True)
    return model


def draw_str(dst, target, s):
    x, y = target
    cv.putText(dst, s, (x + 1, y + 1), cv.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness=2, lineType=cv.LINE_AA)
    cv.putText(dst, s, (x, y), cv.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv.LINE_AA)



