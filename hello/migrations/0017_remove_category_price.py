# Generated by Django 2.2 on 2019-05-23 03:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hello', '0016_auto_20190523_1045'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='price',
        ),
    ]
