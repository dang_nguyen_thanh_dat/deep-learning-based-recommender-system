# import the necessary packages
import json
import os
import random
import sys
import cv2 as cv
import keras.backend as K
import numpy as np
import scipy.io
from django.conf import settings 
# import signal
from .utils import load_model
import gc

def indent(url):
    img_width, img_height = 224, 224
    model = load_model()
    test_path = url
    bgr_img = cv.imread(test_path)
    bgr_img = cv.resize(bgr_img, (img_width, img_height), cv.INTER_CUBIC)
    rgb_img = cv.cvtColor(bgr_img, cv.COLOR_BGR2RGB)
    rgb_img = np.expand_dims(rgb_img, 0)
    preds = model.predict(rgb_img)
    class_id = np.argpartition(preds.flatten(), -4)[-4:]
    class_id = class_id[np.argsort(preds.flatten()[class_id])] 

    results = []
    for i in  class_id.flatten().tolist():
        props = round(preds.flatten()[i]*100,2)
        results.append((i+1,props))
    # for i in reversed(results):
    #     print(i[0])    
    K.clear_session()
    #del model
    #gc.collect()
    #return reversed(class_id.flatten().tolist())
    return reversed(results)
    
