from django.urls import path, include

from django.contrib import admin

admin.autodiscover()

import hello.views

from hello import views
from django.conf import settings
from django.conf.urls.static import static
# To add a new path, first import the app:
# import blog
#
# Then add the new path:
# path('blog/', blog.urls, name="blog")
#
# Learn more here: https://docs.djangoproject.com/en/2.1/topics/http/urls/

urlpatterns = [
    
    path("db/", hello.views.db, name="db"),
    path("admin/", admin.site.urls),
    path("home", hello.views.home, name="home"),  
 	path("search/", hello.views.search, name="search"),   
 	path("Topproduct/", hello.views.Topproduct, name="Topproduct"), 
 	
 	path("detail/<int:id>", hello.views.post_detail, name="post_detail"),
 	
 	path("upload/", hello.views.upload, name="upload"),

 	path("history/<int:idu>", hello.views.history, name="history"),
 	path("Result_history/<int:idi>", hello.views.Result_history, name="Result_history"),

 	path("search/upload/", hello.views.upload, name="searchupload"),

 	# path("search/<int:year>/<int:price>", hello.views.filter_data, name="filter_data"),
 	path("filter/<str:txt>", hello.views.filter, name="filter"),
 	path("", hello.views.login, name="login"),
 	path("signined/", hello.views.signined, name="login"),
 	path("signuped/", hello.views.signuped, name="login"),
 	path("contact/", hello.views.contact, name="login"),
]
if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)